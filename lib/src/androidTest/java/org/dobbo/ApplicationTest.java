/*
 * Copyright (c) 2015 Steve Dobson.  Some rights reserved.
 *
 * This java class is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file; if not, write to:
 *      Free Software Foundation, Inc.,
 *      59 Temple Place,
 *      Suite 330,
 *      Boston, MA 02111-1307
 *      USA
 *
 * If you wish to use this project in a commercial project where the LGPL is
 * not an acceptable license, then you have two choices: 1). do not use this
 * code, or 2). contact me, Steve Dobson, to discuss licensing terms.
 *
 * Steve Dobson <steve@dobbo.org>
 */

package org.dobbo;

import android.app.Application;
import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest
        extends ApplicationTestCase<Application> {

    public ApplicationTest() {
        super(Application.class);
    }
}