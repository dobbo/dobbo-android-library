/*
 * Copyright (c) 2015 Steve Dobson.  Some rights reserved.
 *
 * This java class is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file; if not, write to:
 *      Free Software Foundation, Inc.,
 *      59 Temple Place,
 *      Suite 330,
 *      Boston, MA 02111-1307
 *      USA
 *
 * If you wish to use this project in a commercial project where the LGPL is
 * not an acceptable license, then you have two choices: 1). do not use this
 * code, or 2). contact me, Steve Dobson, to discuss licensing terms.
 *
 * Steve Dobson <steve@dobbo.org>
 */

package org.dobbo.view;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;

import org.dobbo.util.GraphicsUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Useful utilities needed by views.
 *
 * <p>Most of the methods provided by this utility base class map to
 * methods in {@link GraphicsUtilities}.  They are provided to make
 * the configuration of the {@link Paint}s needed by views when rendering
 * simple to configure and alter when needed.  Most of the methods are
 * static to reduce the class linkage.  But one or two of them need
 * context</p>
 *
 * <p>The other utility provided by this class is a system logger.</p>
 *
 * @see GraphicsUtilities
 * @author Steve Dobson &lt;steve&#64;dobbo.org&gt;
 */
@SuppressWarnings("unused")
public abstract class AbstractView
        extends View {

    /** The system logger. */
    protected transient final Logger mLogger;

    /**
     * Instantiate the utility base.
     *
     * @param context The context in which this view operates.
     */
    protected AbstractView(final Context context) {
        super(context);
        mLogger = LoggerFactory.getLogger(getClass());
    }

    /**
     * Instantiate the abstract view.
     *
     * @param context The context in which this view operates.
     * @param attrs   The attributes for the new.
     */
    protected AbstractView(final Context context,
                           final AttributeSet attrs) {
        super(context, attrs);
        mLogger = LoggerFactory.getLogger(getClass());
    }

    /**
     * Instantiate the abstract view.
     *
     * @param context  The context in which this view operates.
     * @param attrs    The attributes for the new.
     * @param defStyle The default style for the new.
     */
    protected AbstractView(final Context context,
                           final AttributeSet attrs,
                           final int defStyle) {
        super(context, attrs, defStyle);
        mLogger = LoggerFactory.getLogger(getClass());
    }

    /**
     * Create new paint of the given color.
     *
     * @param color The color for the new paint.
     *
     * @return Some paint.
     */
    protected static Paint initPaint(final int color) {
        return color(GraphicsUtilities.initPaint(), color);
    }

    @NonNull
    protected static Paint color(@NonNull final Paint paint,
                                 final int color) {
        return GraphicsUtilities.color(paint, color);
    }

    /**
     * Configure the given paint to be of the stroke style.
     *
     * @param paint The paint to be configured.
     * @param width the width of the stroke.
     *
     * @return The paint once configured.
     */
    @NonNull
    protected static Paint stroke(@NonNull final Paint paint,
                                  final float width) {
        return GraphicsUtilities.stroke(paint, width);
    }

    /**
     * Configure the given paint to be of the fill style.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    protected static Paint fill(@NonNull final Paint paint) {
        return GraphicsUtilities.fill(paint);
    }

    /**
     * Configure the given paint to be of the fill and stroke style.
     *
     * @param paint The paint to be configured.
     * @param width the width of the stroke.
     *
     * @return The paint once configured.
     */
    @NonNull
    protected static Paint fillAndStroke(@NonNull final Paint paint,
                                         final float width) {
        return GraphicsUtilities.fillAndStroke(paint, width);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected Paint typeface(@NonNull final Paint paint,
                             @NonNull final String font) {
        return GraphicsUtilities.typeface(getContext(), paint, font);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected static Paint normal(@NonNull final Paint paint) {
        return GraphicsUtilities.normal(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected static Paint bold(@NonNull final Paint paint) {
        return GraphicsUtilities.bold(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected static Paint boldItalic(@NonNull final Paint paint) {
        return GraphicsUtilities.boldItalic(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected static Paint italic(@NonNull final Paint paint) {
        return GraphicsUtilities.italic(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    protected static Paint dashed(@NonNull final Paint paint,
                           float[] intervals,
                           int phase) {
        GraphicsUtilities.dashed(paint, intervals, phase);
        return paint;
    }

    /**
     * Configure the given paint to render any text centered.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    protected static Paint center(@NonNull final Paint paint) {
        return GraphicsUtilities.center(paint);
    }

    /**
     * Configure the given paint to render any text left aligned.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint left(@NonNull final Paint paint) {
        return GraphicsUtilities.left(paint);
    }

    /**
     * Configure the given paint to render any text right aligned.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint right(@NonNull final Paint paint) {
        return GraphicsUtilities.right(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    public static Paint strikeThrough(@NonNull final Paint paint) {
        return GraphicsUtilities.strikeThrough(paint);
    }

    @SuppressWarnings("unused")
    @NonNull
    public static Paint noStrikeThrough(@NonNull final Paint paint) {
        return GraphicsUtilities.noStrikeThrough(paint);
    }

    /**
     * Configure the given paint be transparent.
     *
     * @param paint The paint to be configured.
     * @param alpha The transparency (0 - 255 inclusive).
     *
     * @return The paint once configured.
     */
    @NonNull
    protected static Paint alpha(@NonNull final Paint paint,
                                 final int alpha) {
        return GraphicsUtilities.alpha(paint, alpha);
    }

    /**
     * Configure the given paint to have a butted cap.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint buttCap(@NonNull final Paint paint) {
        return GraphicsUtilities.buttCap(paint);
    }

    /**
     * Configure the given paint to have a squared cap.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint squareCap(@NonNull final Paint paint) {
        return GraphicsUtilities.squareCap(paint);
    }

    /**
     * Configure the given paint to have a rounded joining.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint bevelJoin(@NonNull final Paint paint) {
        return GraphicsUtilities.bevelJoin(paint);
    }

    /**
     * Configure the given paint to have a rounded joining.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint miterJoin(@NonNull final Paint paint) {
        return GraphicsUtilities.miterJoin(paint);
    }

    @NonNull
    protected Paint ptSize(@NonNull final Paint paint,
                           final float size) {
        return GraphicsUtilities.ptSize(getContext(), paint, size);
    }

    protected float ptSize(final float size) {
        return GraphicsUtilities.ptSize(getContext(), size);
    }

    @NonNull
    protected static Paint round(@NonNull final Paint paint) {
        return GraphicsUtilities.round(paint);
    }
}
