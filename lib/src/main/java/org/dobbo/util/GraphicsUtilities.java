/*
 * Copyright (c) 2015 Steve Dobson.  Some rights reserved.
 *
 * This java class is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this file; if not, write to:
 *      Free Software Foundation, Inc.,
 *      59 Temple Place,
 *      Suite 330,
 *      Boston, MA 02111-1307
 *      USA
 *
 * If you wish to use this project in a commercial project where the LGPL is
 * not an acceptable license, then you have two choices: 1). do not use this
 * code, or 2). contact me, Steve Dobson, to discuss licensing terms.
 *
 * Steve Dobson <steve@dobbo.org>
 */
package org.dobbo.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;

/**
 *
 * @author Steve Dobson &lt;steve@dobbo.org&gt;
 */
public class GraphicsUtilities {

    private GraphicsUtilities() {
    }

    /**
     * Create new paint of the given color.
     *
     * @return Some paint.
     */
    @NonNull
    public static Paint initPaint() {
        Paint paint = new Paint(Color.RED);
        paint.setDither(true);
        paint.setAntiAlias(true);
        return paint;
    }

    /**
     * Create new paint of the given color.
     *
     * @param color The color for the new paint.
     *
     * @return Some paint.
     */
    @SuppressWarnings("unused")
    @NonNull
    public static Paint initPaint(final int color) {
        Paint paint = initPaint();
        paint.setColor(color);
        return paint;
    }

    @NonNull
    public static Paint color(@NonNull final Paint paint,
                              final int color) {
        paint.setColor(color);
        return paint;
    }

    /**
     * Configure the given paint to be of the stroke style.
     *
     * @param paint The paint to be configured.
     * @param width the width of the stroke.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint stroke(@NonNull final Paint paint,
                               final float width) {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(width);
        return paint;
    }

    /**
     * Configure the given paint to be of the fill style.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint fillAndStroke(@NonNull final Paint paint,
                                      final float width) {
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(width);
        return paint;
    }

    /**
     * Configure the given paint to be of the fill style.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint fill(@NonNull final Paint paint) {
        paint.setStyle(Paint.Style.FILL);
        return paint;
    }

    /**
     * Configure the given paint to have a rounded cap and join.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint round(@NonNull final Paint paint) {
        return roundJoin(roundCap(paint));
    }

    /**
     * Configure the given paint to have a rounded cap.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint roundCap(@NonNull final Paint paint) {
        paint.setStrokeCap(Paint.Cap.ROUND);
        return paint;
    }

    /**
     * Configure the given paint to have a butted cap.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint buttCap(@NonNull final Paint paint) {
        paint.setStrokeCap(Paint.Cap.BUTT);
        return paint;
    }

    /**
     * Configure the given paint to have a squared cap.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint squareCap(@NonNull final Paint paint) {
        paint.setStrokeCap(Paint.Cap.SQUARE);
        return paint;
    }

    /**
     * Configure the given paint to have a rounded joining.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint bevelJoin(@NonNull final Paint paint) {
        paint.setStrokeJoin(Paint.Join.BEVEL);
        return paint;
    }

    /**
     * Configure the given paint to have a rounded joining.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint miterJoin(@NonNull final Paint paint) {
        paint.setStrokeJoin(Paint.Join.MITER);
        return paint;
    }

    /**
     * Configure the given paint to have a rounded joining.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint roundJoin(@NonNull final Paint paint) {
        paint.setStrokeJoin(Paint.Join.ROUND);
        return paint;
    }

    /**
     * Configure the given paint to render any text at the given size.
     *
     * @param paint The paint to be configured.
     * @param size  The size of the text (in pixels).
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint textSize(@NonNull final Paint paint,
                                 final float size) {
        paint.setTextSize(size);
        return paint;
    }

    /**
     * Configure the given paint to render any text centered.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint center(@NonNull final Paint paint) {
        paint.setTextAlign(Paint.Align.CENTER);
        return paint;
    }

    /**
     * Configure the given paint to render any text left aligned.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint left(@NonNull final Paint paint) {
        paint.setTextAlign(Paint.Align.LEFT);
        return paint;
    }

    /**
     * Configure the given paint to render any text right aligned.
     *
     * @param paint The paint to be configured.
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint right(@NonNull final Paint paint) {
        paint.setTextAlign(Paint.Align.RIGHT);
        return paint;
    }

    /**
     * Configure the given paint be transparent.
     *
     * @param paint The paint to be configured.
     * @param alpha The transparency (0 - 255 inclusive).
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint alpha(@NonNull final Paint paint,
                              final int alpha) {
        paint.setAlpha(Math.max(0, Math.min(alpha, 255)));
        return paint;
    }

    @NonNull
    public static Paint strikeThrough(@NonNull final Paint paint) {
        paint.setFlags(paint.getFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return paint;
    }

    @NonNull
    public static Paint noStrikeThrough(@NonNull final Paint paint) {
        paint.setFlags(paint.getFlags() & ~Paint.STRIKE_THRU_TEXT_FLAG);
        return paint;
    }

    /**
     * Configure the given paint to render any text at the given point size.
     *
     * @param paint The paint to be configured.
     * @param size  The size of the text (in points).
     *
     * @return The paint once configured.
     */
    @NonNull
    public static Paint ptSize(@NonNull final Context context,
                               @NonNull final Paint paint,
                               final float size) {
        return textSize(paint, ptSize(context, size));
    }

    @NonNull
    public static Paint typeface(@NonNull final Context context,
                                 @NonNull final Paint paint,
                                 @NonNull final String font) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), font);
        if (null != typeface) {
            paint.setTypeface(typeface);
        }
        return paint;
    }

    @NonNull
    public static Paint normal(@NonNull final Paint paint) {
        Typeface tf = Typeface.create(paint.getTypeface(), Typeface.NORMAL);
        paint.setTypeface(tf);
        return paint;
    }

    @NonNull
    public static Paint bold(@NonNull final Paint paint) {
        Typeface tf = Typeface.create(paint.getTypeface(), Typeface.BOLD);
        paint.setTypeface(tf);
        return paint;
    }

    @NonNull
    public static Paint boldItalic(@NonNull final Paint paint) {
        Typeface tf = Typeface.create(paint.getTypeface(), Typeface.BOLD_ITALIC);
        paint.setTypeface(tf);
        return paint;
    }

    @NonNull
    public static Paint italic(@NonNull final Paint paint) {
        Typeface tf = Typeface.create(paint.getTypeface(), Typeface.ITALIC);
        paint.setTypeface(tf);
        return paint;
    }

    @NonNull
    public static Paint dashed(@NonNull final Paint paint,
                                float[] intervals,
                                int phase) {
        paint.setPathEffect(new DashPathEffect(intervals, phase));
        return paint;
    }

    /**
     * Determine the number of pixel for a given point size.
     *
     * @param context A content.
     * @param size    The size (in points).
     *
     * @return A length in pixel.
     */
    public static float ptSize(@NonNull final Context context,
                               final float size) {
        final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return size * metrics.densityDpi / 72f;
    }
}
